import React from "react";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import About from "../pages/About"
import Home from "../pages/Home";
import Equipement from "../pages/Equipement";
import Login from "../pages/Login";

import logo from "../images/logo1.png"


const App = () => {
  return (
    <BrowserRouter>
      <div className="menu">
        <div className="logo">
          <img src={logo} alt="Go Securis" />
        </div>
        <Link to="/" className="btn">Home</Link>

        <Link to="/about" className="btn">About</Link>

        <Link to="/equipement" className="btn">Equipement</Link>

        <Link to="/login" className="btn">Login</Link>
      </div>

      <Routes>
        <Route path="/" element={<Home />} />

        <Route path="/about" element={<About />} />

        <Route path="/equipement" element={<Equipement />} />

        <Route path="/login" element={<Login />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
