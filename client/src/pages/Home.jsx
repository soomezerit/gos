import React from "react";
import { Link } from "react-router-dom";
import logo from "../images/logo1.png"

const Home = () => {
  return (
    <main className="home">
      <div>
        <img src={logo} alt="Go Securis" />
        <div className="card">
          <h1>Welcome to Go Securi App</h1>

          <p>Suspendisse ac condimentum elit. Etiam id mauris at diam tincidunt sodales.
            Maecenas ac mollis magna, et lobortis diam. Etiam eu dui bibendum.
          </p>

          <Link to="/login" className="btn">Login</Link>

        </div>
        
      </div>
    </main>
  );
};


export default Home;
