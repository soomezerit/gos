import React from "react";

import logo from "../images/logo1.png"

const About = () => {
  return (
    <main>
      <div>
        <h1>About</h1>
        <p>Suspendisse ac condimentum elit. Etiam id mauris at diam tincidunt sodales.
          Maecenas ac mollis magna, et lobortis diam. Etiam eu dui bibendum, aliquet felis a,
          lacinia diam. In sit amet ipsum fringilla, dictum sapien nec, imperdiet metus. Maecenas
          rutrum quam ac arcu porttitor vestibulum. Nullam sed nunc ex. Vivamus hendrerit malesuada felis,
          a sodales mi bibendum in. Vestibulum rutrum luctus consequat. Suspendisse id maximus sapien, quis venenatis enim.
          Quisque sagittis orci vitae metus faucibus semper.
        </p>
        <img src={logo} alt="Go Securis" />
        <h2>titre 2</h2>
        <p>Donec luctus dolor sed ornare iaculis. Quisque ultrices condimentum augue,
          non interdum arcu. Nunc accumsan lorem magna, id fringilla purus interdum vestibulum.
          Sed dolor mauris, vulputate vitae imperdiet commodo, aliquet vel dolor. Ut at nibh sed quam
          dictum dignissim. Aenean hendrerit sit amet eros eget iaculis. Proin nisl elit, imperdiet in arcu quis,
          imperdiet euismod libero. Nullam nisl urna, molestie eu dapibus non, dictum non lorem. Cras volutpat sem ac metus aliquet,
          rutrum ullamcorper arcu congue. Nunc dapibus placerat lectus. Maecenas neque nunc, pharetra id lacus id, blandit sollicitudin risus.
          Suspendisse ullamcorper hendrerit est, id finibus mauris malesuada ut. Curabitur eget lorem euismod, ultricies metus vitae, dapibus dolor.</p>
        
        <h3>titre 3</h3>
        <p>Donec luctus dolor sed ornare iaculis. Quisque ultrices condimentum augue,
          non interdum arcu. Nunc accumsan lorem magna, id fringilla purus interdum vestibulum.
          Sed dolor mauris, vulputate vitae imperdiet commodo, aliquet vel dolor. Ut at nibh sed quam
          dictum dignissim. Aenean hendrerit sit amet eros eget iaculis. Proin nisl elit, imperdiet in arcu quis,
          imperdiet euismod libero. Nullam nisl urna, molestie eu dapibus non, dictum non lorem. Cras volutpat sem ac metus aliquet,
          rutrum ullamcorper arcu congue. Nunc dapibus placerat lectus. Maecenas neque nunc, pharetra id lacus id, blandit sollicitudin risus.
          Suspendisse ullamcorper hendrerit est, id finibus mauris malesuada ut. Curabitur eget lorem euismod, ultricies metus vitae, dapibus dolor.</p>
      </div>
    </main>
  );
};

export default About;
