import React, { useState, useEffect } from 'react';

class Equipement extends React.Component {


  constructor(props) {
    super(props);

    this.equimentTaken = {
      ba : false,
      bl : false, 
      bs : false, 
      cm : false, 
      cs : false,
      cv : false,
      dm : false,
      gi : false,
      gp : false,
      ko : false,
      lt : false,
      ms : false,
      ta : false,
      tw: false,
    }

    this.state = {
      equipments: [],
    };
    this.userstate = {
      users: [],
    }

    this.getEquipments = this.getEquipments.bind(this);
    this.getUsers = this.getUsers.bind(this);
  }
  
  async getEquipments() {
    const req = await fetch('http://localhost:3000/equipments');
    const data = await req.json();
    console.log(data.equipments)
    return data.equipments
    

  }

  async componentDidMount() {
    const equipments = await this.getEquipments();
    this.setState({ equipments });
  }

  hangleCheckbox(){

  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  

  render() {
    return (
      <main>
        <div className="login">
          <h1>Bonjour {users.name}</h1>

          <img className="oopsy" src={users.src} alt="Photo Michel" />

          <div className="card">
            <h2>Equipement disponible</h2>
            <p>Veuillez cocher les equipements que vous désirez prendre</p>
            <form method='POST'>
            <ul>
              {
                this.state.equipments.map((equipment) => (
                  

                  <li><label>{equipment.maxQ}-{equipment.name}<input type="checkbox" name={equipment.id} checked={this.equimentTaken.equipment.id}/></label></li>
                )

                )}
            </ul>
            <button className="btn " type='submit'>Prendre</button>
            </form>
          </div>
        </div>
      </main>

    );

  }
}

export default Equipement;
