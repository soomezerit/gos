import React, { useEffect, useRef } from "react";
import axios from 'axios';

const Login = () => {
  const videoRef = useRef(null);
  const photoRef = useRef(null);
  const stripRef = useRef(null);

  useEffect(() => {
    getVideo();
  }, [videoRef]);

  const getVideo = () => {
    navigator.mediaDevices
      .getUserMedia({ video: { width: 640 } })
      .then(stream => {
        let video = videoRef.current;
        video.srcObject = stream;
        video.play();
      })
      .catch(err => {
        console.error("error:", err);
      });
  };

  const paintToCanvas = () => {
    let video = videoRef.current;
    let photo = photoRef.current;
    let ctx = photo.getContext("2d");

    const width = 800;
    const height = 600;
    photo.width = width;
    photo.height = height;

    return setInterval(() => {
      ctx.drawImage(video, 0, 0, width, height);
    }, 200);
  };

  const takePhoto = () => {
    let photo = photoRef.current;
    let strip = stripRef.current;

    console.warn(strip);

    const data = photo.toDataURL("image/jpeg");

    console.warn(data);
    const link = document.createElement("a");
    link.href = data;
    link.setAttribute("download", "myWebcam");
    link.innerHTML = `<img src='${data}' alt='thumbnail'/>`;
    strip.insertBefore(link, strip.firstChild);




    const imageTaken = {image:data};
    const config = {headers: {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': "*", "Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS", "Authorization":"X-Requested-With"}}
    axios.post('http://localhost:3000/imageTaken', imageTaken, config)
      .then(response => this.setState({ imageCorrespondance: response.data.id }));


  };

  return (
    <main>
      <div className="login">
        <h1>Welcome to the GO SECURIS login page.</h1>
        <div className="hidden">
          <video onCanPlay={() => paintToCanvas()} ref={videoRef} />
        </div>
        <div>

          <canvas ref={photoRef} />
          <p> Instructions d'utilisation</p>
          <a className="btn" onClick={() => takePhoto()}>Detect</a>
          
        </div>
        <div className="strip" ref={stripRef} />
      </div>
    </main >
  );
};
export default Login;
