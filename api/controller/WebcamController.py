
from json import tool
from aiohttp import request
import cv2
import urllib.request
import numpy as np
import requests

from generator import WebcamGenerator
from generic import Tools

class WebcamController(object):

    def __init__(self):
        self.imageReference = "Path To Image Reference"
        self.minimum_image_diff = 0.7
        self.storagePath = "http://localhost:5000"
        self.cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
        

    '''Function that Start the Face Detection'''
    def launchFaceDetection(self):

        ig = WebcamGenerator.WebcamGenerator()
        
        tools = Tools.Tools()

        frame = tools.getWebcamImage()
        
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces, detected = self.detect(gray, self.cascade)

        for (x, y, w, h) in faces:            
            faces = frame[y:y + h, x:x + w]
                

        if detected != 0 :
            print("[INFO] Face Detected !")

            req = ig.getStorage()
            print(req)
            for data in req.get("files"):
                #print(data)
                #print(data)
                buildPathToImg =  self.storagePath + "/upload/" + data
                buildPathToImg = urllib.request.urlopen(buildPathToImg)

                img_array = np.array(bytearray(buildPathToImg.read()), dtype=np.uint8)

                imageReference = cv2.imdecode(img_array, -1)

                print("[INFO] Comparing with Database..... !")
                diff = self.comparing(faces, imageReference)

                print(diff)

                if diff != None:
                    final = True
                    break
                        

            return final, data

        else:
            print("[ERROR] No Face detected!")


    '''Function Detecting Faces using the Haar Cascade Classifier'''
    def detect(self, grayImg, cascade):

        faces = cascade.detectMultiScale(grayImg, 
                                                scaleFactor=1.3,
                                                minNeighbors=3,
                                                minSize=(30, 30))

        detected = len(faces)

        return faces, detected
    

    '''Function Comparing Detected Face with the image referenced'''
    def comparing(self, imageOne, imageTwo):
        
        commutative_image_diff = self.differencing(imageOne, imageTwo)
        #print(commutative_image_diff)

        if commutative_image_diff > self.minimum_image_diff:
            #print("[INFO] Face Match !")
            return commutative_image_diff
        else:
            print("[INFO] Face Not Recognize !")

    ''''Function Calculating the difference between two image using '''    
    def differencing(self, imageOne, imageTwo):
        imageOne = cv2.calcHist([imageOne], [0], None, [256], [0, 256])
        imageTwo = cv2.calcHist([imageTwo], [0], None, [256], [0, 256])

        probabilityMatch = cv2.matchTemplate(imageOne, imageTwo, cv2.TM_CCOEFF_NORMED)[0][0]

        print('Probabilité de correspondance: {0}'.format(probabilityMatch))

        return probabilityMatch