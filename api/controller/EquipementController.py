
from flask import request
from generator.EquipementGenerator import EquipementGenerator
from generator.UserGenerator import UserGenerator
import urllib.request
import cv2
import numpy as np

from generic.Tools import Tools

class EquipementController(object):

    def __init__(self):
        self.storagePath = "http://localhost:5000"

    def oneForList(self, image):
        ug = UserGenerator()
        tl = Tools()

        equipmentList = self.listAllEquipement()
        userList = ug.getAllUsers()

        for user in userList["users"]:
            if user.get("id") == image:
                userfinal = user
                userequiment = user.get("equipments")
                break
            else:
                print('User not in DB')

        equipementActual, equipmentListWithoutActual =  tl.listDifference(equipmentList, userequiment)

        username = userfinal.get('id')
        username = username.replace('.jpg','')
        username = username.replace('-',' ')

        profilPic =  self.storagePath + "/upload/" + userfinal.get('id')

        return equipmentListWithoutActual, userfinal, equipementActual, username, profilPic

    def listAllEquipement(self):
        eqg = EquipementGenerator()
        req = eqg.getAllEquipements()
        return req

    def takeEquipement(self, obj):
        self.eg.EquipementGenerator.getEquipement(obj)

    def returnEquipement(self, obj):
        self.eg.EquipementGenerator.putEquipement(obj)

    def equipementResume(self, obj):
        ug = UserGenerator()
        eqg = EquipementGenerator()
        listToPut = []
        user = ""
        final = {}
        userList = ug.getAllUsers()

        for data in obj:
            for user in userList["users"]:
                if user.get('id') == data:
                 userId = data

        for info in obj:
            if info == userId:
                user = userId
            else:
                listToPut.append(info)
        
        final["equipements"]=listToPut
        final["userId"]=userId
        eqg.putEquipement(final)



