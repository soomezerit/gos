import imp
import cv2
import numpy as np
from controller.WebcamController import WebcamController

def main():

    webC = WebcamController()

    webC.launchFaceDetection()

if __name__ == "__main__":
    main()