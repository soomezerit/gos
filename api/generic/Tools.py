import cv2

class Tools(object):
    '''
    classdocs
    '''

    def getWebcamImage(self):

        webcam = cv2.VideoCapture(0)

        (_, frame) = webcam.read()

        return frame
        
    def listDifference (self, list1, list2):
        

        equipmentActual= []
        equipmentListWithoutActual=[]

        for data in list1["equipments"]:
            if data.get("id") in list2:
                equipmentActual.append(data)
            else:
                equipmentListWithoutActual.append(data)
        #print(equipmentListWithoutActual)
        return equipmentActual, equipmentListWithoutActual