from importlib.resources import path
import requests


class EquipementGenerator(object):

    def __init__(self):
        self.path="http://localhost:5000"

    def getAllEquipements(self):
        req = requests.get(self.path + '/equipments')
        req = req.json()
        return req

    def putEquipement(self, data):
        print(data)
        requests.put(self.path+'/users', json = data)