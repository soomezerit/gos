from importlib.resources import path
import requests


class WebcamGenerator(object):

    def __init__(self):
        self.path="http://localhost:5000"

    def getStorage(self):
        req = requests.get(self.path + '/upload')
        req = req.json()
        return req

    def getImage(self, image):
        req = requests.get(self.path + '/upload/' + image)
