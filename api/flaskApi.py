
from aiohttp import request
import cv2
from flask import Flask, Response, render_template, request
from flask_cors import CORS


from controller.WebcamController import WebcamController
from controller.UserController import UserController
from controller.EquipementController import EquipementController


app = Flask(__name__, template_folder='templates')
cors = CORS(app, resources={"/api/*": {"origins": "*"}})



def generate_frames():

    camera = cv2.VideoCapture(0)
    while True:
        
        ## read the camera frame
        success,frame=camera.read()
        if not success:
            break
        else:
            ret,buffer=cv2.imencode('.jpg',frame)
            frame=buffer.tobytes()

        yield(b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
    camera.release()

@app.route('/')
def index():
    
    return render_template("index.html")

@app.route('/login')
def login():
    return render_template("login.html")

@app.route('/video')
def video():
    return Response(generate_frames(),mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/equipements')
def scan():
    try:
        wb = WebcamController()
        eq = EquipementController()
        detect, image = wb.launchFaceDetection()

        eq = EquipementController()
        equiList, user, equipmentActual, username, profilPic = eq.oneForList(image)

        


        print(user)
        if detect == True :
            return render_template('equipements.html', list=equiList, user=user, equipmentActual=equipmentActual, username=username, profilPic=profilPic)
        else:
            return render_template("error.html") 
    except:
        return render_template("error.html")

@app.route('/resume', methods=["POST"])
def sended():
    eq = EquipementController()
    data = request.form
    eq.equipementResume(data)
    return render_template('resume.html')




@app.route('/test')
def test():
    eq = EquipementController()
    list = eq.oneForList()
    print(list)
    #return render_template('equipements.html', list =list)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port="3000")